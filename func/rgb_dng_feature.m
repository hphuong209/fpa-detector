function [nlv_noise,nlv_img,skin,smooth_esp,content_var] = rgb_dng_feature(image_name,color_option)
    B = 8;
    %% read file
    img = openRawImage(image_name,'8-rgb-raw');
    rgb = openRawImage(image_name,'8-rgb');
    x = floor(size(img,1)/B);
    y = floor(size(img,2)/B);
    img = img(1:B*x,1:B*y,:);
    rgb = rgb(1:B*x,1:B*y,:);
    %% Skin mask
    skin_map = skinmap(rgb);
    %% Color Channel Selection
    if color_option == 0
        img = rgb2gray(img);
    else
        img = img(:,:,color_option);
    end
    img =  im2double(img); %% A voir double ou im2double
    %% Denoising
    sigma = function_stdEst2D(img,4);
    noise = noiseextractfromgrayscale(img,sigma*4);
    denoised = img-noise;
    smooth_denoised = imgaussfilt(denoised,3);
    %% Selection Mask production

    skin = reshape(sum(im2col(skin_map,[B B],'distinc'))>32,x,y);
    content_var = reshape(var(im2col(denoised,[B B],'distinc')),x,y);
    smooth_esp = reshape(mean(im2col(smooth_denoised,[B B],'distinc')),x,y);
    nlv_noise = reshape(var(im2col(noise,[B B],'distinc')),x,y);
    nlv_img = reshape(var(im2col(img,[B B],'distinc')),x,y);
 end




