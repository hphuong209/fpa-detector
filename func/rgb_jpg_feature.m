function [nlv_noise,nlv_img,skin,smooth_esp,content_var] = rgb_jpg_feature(image_name,color_option,opt)
    B = 8;
    %% read file
    img = imread(image_name);
    x = floor(size(img,1)/B);
    y = floor(size(img,2)/B);
    img = img(1:B*x,1:B*y,:);
    %% Skin mask
    skin_map = skinmap(img);
    %% Color Channel Selection
    img =  im2double(img);
    if opt~=0
        if opt==-1
           tmp =imfinfo(image_name);
           gamma_coef = cell2mat(tmp.Comment);
        else
           gamma_coef = opt;
        end
        img = img.^str2num(gamma_coef);
    end
    if color_option == 0
        img = rgb2ycbcr(img);
        img = img(:,:,1);
    else
        img = img(:,:,color_option);
    end
    %% Denoising
    sigma = function_stdEst2D(img,4);
    noise = noiseextractfromgrayscale(img,sigma*4);
    denoised = img-noise;
    smooth_denoised = imgaussfilt(denoised,3);
    %% Selection Mask production

    skin = reshape(sum(im2col(skin_map,[B B],'distinc'))>32,x,y);
    content_var = reshape(var(im2col(denoised,[B B],'distinc')),x,y);
    smooth_esp = reshape(mean(im2col(smooth_denoised,[B B],'distinc')),x,y);
    nlv_noise = reshape(var(im2col(noise,[B B],'distinc')),x,y);
    nlv_img = reshape(var(im2col(img,[B B],'distinc')),x,y);
 end




