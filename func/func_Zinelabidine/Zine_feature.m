function [ feature ] = Zine_feature( crop_file, desc_type)
%ZINE_FEATURE Summary of this function goes here
%   Detailed explanation goes here
%    crop_file = '/media/davy/Davy/RAW_FACE/nikon-phuong/real/JPG/crop/img (1).JPG';
%     desc_type = string('BSIF');

   rgb = imread(crop_file);
   yCbCr = rgb2ycbcr(rgb);
   if desc_type == string('LBP')
       filtR=generateRadialFilterLBP(8,1);
       ref8 = uint8(0:2^8-1);
       refBase2 = dec2base(ref8,2);
       refBase2_bis = refBase2(:,[2:end,1]);
       ref8 = ref8(sum(abs(refBase2-refBase2_bis),2)<=2);
       feature = [];
       for i=1:3
           lbp = uniformLBP(efficientLBP(yCbCr(:,:,i),'filtR',filtR,'isRotInv', false),8);           
           [a,b] = hist(lbp,unique(lbp));
            if length(b)==(length(ref8)+1)
                lbp_hist=a;
            else
                lbp_hist = zeros(1,length(ref8)+1);
                for j=1:length(b)
                    lbp_hist(ref8==b(j))=a(j);
                end
                if b(end) == 2^8
                    lbp_hist(end) = a(end);
                end
            end
            feature =[feature lbp_hist];            
       end
   elseif desc_type == string('CoALBP')
       feat_func = @(img) [cvtCoALBP(img,1,2); cvtCoALBP(img,2,4); cvtCoALBP(img,4,8)]';      
   elseif desc_type == string('LPQ')
       feat_func = @(img)LPQdesc(img,7);
   elseif desc_type == string('BSIF')
       feature=[];
       for i=1:3
           img = yCbCr(:,:,i);
           for j=7
               load(strcat('ICAtextureFilters_7x7_',num2str(j),'bit.mat'),'ICAtextureFilters');
               feature = [feature bsif(img,ICAtextureFilters)];
           end
       end
   elseif desc_type == string('SID')
   end
   
   if exist('feat_func')
        feature=[feat_func(yCbCr(:,:,1)) feat_func(yCbCr(:,:,2)) feat_func(yCbCr(:,:,3))];
   end
%end

%TODO : histogram