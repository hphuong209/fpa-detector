clear;
clc;

%% init
foo = @(img) [cvtCoALBP(img,1,2); cvtCoALBP(img,2,4); cvtCoALBP(img,4,8)];
input = 'sample.jpg'; 
img = rgb2gray(imread(input));

%% feature extraction
F = foo(img);

%% vilualization
figure(1);  imshow(img);
figure(2);  bar(F);
