BSIF in GPU for Matlab:  (requires 'Parallel Computing Toolbox')

This code extends the original BSIF (binarized statistical image features) function 
to be computed utilizing a GPU in Matlab


This is an extension for the BSIF code published in:
Kannala J & Rahtu E: "BSIF: binarized statistical image features", ICPR 2012.

The original code (CPU only) can be found in:
http://www.ee.oulu.fi/~jkannala/bsif/bsif.html


Usage:

Call the function normally using at least one argument as a gpuArray:


Examples:

--------------
- CPU usage


bsifhistnorm = bsif(img,ICAtextureFilters,'nh');     --> computes bsif code using CPU



* This is equivalent to the original code

--------------


- GPU usage


gimg = gpuArray(img);      -->   copy image to GPU memory

gbsifhistnorm = bsif(gimg, ICAtextureFilters,'nh');   -->compute bsif code using GPU

bsifhistnorm = gather(gbsifhistnorm); %readback descriptor from GPU memory  --> read back from GPU memory if needed



* This uses the GPU to do the computations using single precision floating point numbers

----------------


If you use this code in your research, please cite:

- Bordallo L�pez M., Nieto A., Boutellier J., Hannuksela J., and Silv�n O.
  "Evaluation of real-time LBP computing in multiple architectures,"
  Journal of Real Time Image Processing, 2014.

or

- Hadid A., Ylioinas J., Bordallo L�pez M.,
 "Face and Texture Analysis Using Local Descriptors: A Comparative Analysis"
  IEEE Int. Conf. Image Processing Theory, Tools and Applications, Paris, France, 2014


------

Miguel Bordallo Lopez, D. Sc.
Center for Machine Vision Research,
University of Oulu, Finland

e-mail: miguelbl[AT]ee.oulu.fi , http://www.ee.oulu.fi/~miguelbl/


