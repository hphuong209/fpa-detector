function [ output ] = uniformLBP( input,base )
%UNIFORMLBP Summary of this function goes here
%   Detailed explanation goes here
    input = double(input(:));
    output = input;
    inputBase2 = dec2base(input,2);
    inputBase2_bis = inputBase2(:,[2:end,1]);
    output(sum(abs(inputBase2-inputBase2_bis),2)>2)=2^base;
 end

