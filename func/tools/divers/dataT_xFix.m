function [out] = dataT_xFix(data,xcor)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    step = xcor(2)-xcor(1);
    bornes = xcor - step/2;
    group = ones(size(data));
    for i=2:length(bornes)
        group = group + double(data>bornes(i));
    end
    ind = unique(group);
    
    
    %ycor = accumarray(group,1)';
%     ycor = ycor(ycor~=0);
%     ycor=ycor/sum(ycor.*step); 
%     out = zeros(1,length(xcor));
%     out(unique(group))= ycor;
    out = zeros(1,length(xcor));
    for i = ind
        out(i) = sum(group==i);
    end
    out=out/sum(out.*step); 
end

