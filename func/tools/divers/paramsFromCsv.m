function [] = paramsFromCsv( csvfile, ind )
%PARAMSFROMCSV Summary of this function goes here
%   Detailed explanation goes here
%csvfile= 'database_configuration.csv';
    fid = fopen(csvfile,'r');
    ctr = 1;
    tline = fgetl(fid);
    A(1,:) = regexp(tline, '\,', 'split'); 
    while(~feof(fid))
        if ischar(tline)
              ctr = ctr + 1;
              tline = fgetl(fid);
              A(ctr,:) = regexp(tline, '\,', 'split'); 
        else
            break;
        end
    end
    fclose(fid);
    ind = find(strcmp(A(3:end,1),num2str(ind)))+2;
    for j =2:size(A,2);
        if(strcmp(A{2,j},'num'))
            assignin('base', A{1,j},str2num(A{ind,j}));
        end
        if(strcmp(A{2,j},'str'))
            assignin('base', A{1,j},A{ind,j});
        end
        if(strcmp(A{2,j},'array_str'))
            assignin('base', A{1,j},regexp(A{ind,j}, '#', 'split'));
        end
        if(strcmp(A{2,j},'array_num'))
            assignin('base', A{1,j},str2num(strrep(A{ind,j}, '#', ',')));
        end
    end
end

