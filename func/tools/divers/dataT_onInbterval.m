function [ out_xcor, out_ycor ] = dataT_onInbterval( data,density,m,M )
%DATAT_ONINBTERVAL Summary of this function goes here
%   Detailed explanation goes here
    step = (M-m)/density;
    bornes = m+step.*(1:(density-1)); 
    group = ones(size(data));
    for i=(density-1):-1:1
        group = group + double(data>bornes(i));
    end
%     xcor = accumarray(group,data,[],@mean);
    out_xcor = linspace(m+step/2,M-step/2,density);
    
    ycor = accumarray(group,1);    
    out_ycor = zeros(density,1);    
    out_ycor(unique(group)) = ycor(ycor~=0);  
    out_ycor = medfilt1(out_ycor,5);
    out_ycor = out_ycor/sum(ycor*step);      
end

