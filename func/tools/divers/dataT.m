function [xcor, ycor] = dataT(data,density)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    m= min(data);
    M= max(data);
    step = (M-m)/density;
    bornes = m+step.*(1:(density-1)); 
    group = ones(size(data));
    for i=(density-1):-1:1
        group = group + double(data>bornes(i));
    end
    xcor = accumarray(group,data,[],@mean);
    ycor = accumarray(group,1);
    xcor= xcor(xcor~=0);
    ycor= ycor(ycor~=0);
    xcor_bis=[xcor(2:end);xcor(end)];    
    ycor=ycor/sum(ycor.*(xcor_bis-xcor));  

end

