function theRawImage = openRaw( theFilename, option)
    VERBAL ='';
    if strcmp(option,'8-rgb')
       system( ['dcraw ',VERBAL,' -T ''', theFilename,'''']);
    elseif strcmp(option,'8-rgb-raw')
       system( ['dcraw ',VERBAL,' -T -w -g 1 1 ''', theFilename,'''']);
    elseif strcmp(option,'raw')
       system( ['dcraw ',VERBAL,' -4 -D -T ''', theFilename,'''']);
    else
        system( ['dcraw ',VERBAL,' ',option,' ''', theFilename,'''']);
    end
    myDateinameZwischenbild = [theFilename( 1:end-3), 'tiff'];
    theRawImage = imread (myDateinameZwischenbild);
    delete( myDateinameZwischenbild);
