function [theRawImage, theTotalFileNameOut] = openRawImage( theTotalFileName, myoption)
%Usage: [theCamRGBImage, theRawImage, theTotalFileName] = openRawImage( theTotalFileName);
%Optional input: theTotalFileName
%Optional output: theTotalFileNameOut
%Description: loads raw image
    if exist( 'theTotalFileName') %#ok<EXIST>
        myTotalFileName = theTotalFileName;
    else
        %Bildauswahl:
        [myTotalFileName, myStatus] = getRawFile4Read( '*.*');
        if myStatus==0
            return;
        end
    end
    
    if exist('myoption') %#ok<EXIST>
        option = myoption;
    else
        %Bildauswahl:
        option ='8-rgb';
    end
    
    theRawImage = openRaw( myTotalFileName,option);

    if nargout > 1
        theTotalFileNameOut = myTotalFileName;
    end