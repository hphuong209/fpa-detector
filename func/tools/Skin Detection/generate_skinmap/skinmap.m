function [ mask ] = skinmap( img_orig )
%SKINMAP Summary of this function goes here
%   Detailed explanation goes here
      %Read the image, and capture the dimensions
      
    img_orig = imgaussfilt(img_orig,5);
    height = size(img_orig,1);
    width = size(img_orig,2);
    
    bin = zeros(height,width);
    mask= zeros(height,width);
    
    %Apply Grayworld Algorithm for illumination compensation
    img = grayworld(img_orig);    
    
    %Convert the image from RGB to YCbCr
    img_ycbcr = rgb2ycbcr(img);
    Cb = img_ycbcr(:,:,2);
    Cr = img_ycbcr(:,:,3);
    %
    mask = Cb>=77 & Cb<=127 & Cr>=133 & Cr<=173; 

end

