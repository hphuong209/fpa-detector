function [S]=skin_detecte(I,opt)
    if opt==1
        I=double(I);
        H=size(I,1);
        W=size(I,2);
        R=I(:,:,1);
        G=I(:,:,2);
        B=I(:,:,3);
        YCbCr=rgb2ycbcr(I);
        Y=YCbCr(:,:,1);
        minY=min(min(Y));
        maxY=max(max(Y));
        Y=255.0*(Y-minY)./(maxY-minY);
        YEye=Y;
        Yavg=sum(sum(Y))/(W*H);

        T=1;
        if (Yavg<64)
            T=1.4;
        elseif (Yavg>192)
            T=0.6;
        end

        if (T~=1)
            RI=R.^T;
            GI=G.^T;
        else
            RI=R;
            GI=G;
        end

        C=zeros(H,W,3);
        C(:,:,1)=RI;
        C(:,:,2)=GI;
        C(:,:,3)=B;
        YCbCr=rgb2ycbcr(C);
        Cr=YCbCr(:,:,3);
       [x y z]= size(YCbCr);
        S=zeros(H,W);
        [SkinIndexRow,SkinIndexCol] =find(10<Cr & Cr<45);
        for i=1:length(SkinIndexRow)
            S(SkinIndexRow(i),SkinIndexCol(i))=1;
        end
    end
    if opt==2
        %% Skin likelihood
        B=8;
        seuil=10;
        Sp = computeSkinProbability(I);
        Sp = (Sp>0);  
        [m n]=size(Sp);
        m=floor(m/B);
        n=floor(n/B);
        Sp=Sp(1:m*B,1:n*B);
        for i = 1:m
            for j = 1:1:n
                if sum(sum(Sp(i*B-B+1:i*B,j*B-B+1:j*B)))<seuil
                    Sp(i*B-B+1:i*B,j*B-B+1:j*B)=zeros(B,B);
                else
                    Sp(i*B-B+1:i*B,j*B-B+1:j*B)=ones(B,B);
                end
            end
        end
        S=imfill(Sp,'holes');
    end
end