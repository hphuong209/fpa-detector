function [ LBP_feature ] = LBPfeature( img )
%LBPFEATURE Summary of this function goes here
 %   Detailed explanation goes here
    if (size(img,3)==3)
        img = rgb2gray(img);
    end
% REF 8
    ref8 = uint8(0:2^8-1);
    refBase2 = dec2base(ref8,2);
    refBase2_bis = refBase2(:,[2:end,1]);
    ref8 = ref8(sum(abs(refBase2-refBase2_bis),2)<=2);
 % REF 16
    ref16 = uint16(0:2^16-1);
    refBase2 = dec2base(ref16,2);
    refBase2_bis = refBase2(:,[2:end,1]);
    ref16 = ref16(sum(abs(refBase2-refBase2_bis),2)<=2);
% % LBP 16,2
    filtR=generateRadialFilterLBP(16,2);
    LBP_vector1 = efficientLBP(img,'filtR',filtR,'isRotInv', false);
    LBP_vector1 = uniformLBP(LBP_vector1,16);
    [a1,b1] = hist(LBP_vector1,unique(LBP_vector1));
    if length(b1)==(length(ref16)+1)
        c1=a1;
    else
        c1 = zeros(1,length(ref16)+1);
        for i=1:length(b1)
            c1(ref16==b1(i))=a1(i);
        end
        if b1(end) == 2^16
            c1(end) = a1(end);
        end
            
    end
    
% LBP 8,2
    filtR=generateRadialFilterLBP(8,2);
    LBP_vector3 = efficientLBP(img,'filtR',filtR,'isRotInv', false);
    LBP_vector3 = uniformLBP(LBP_vector3,8);
    [a3,b3] = hist(LBP_vector3,unique(LBP_vector3));
    if length(b3)==(length(ref8)+1)
        c3=a3;
    else
        c3 = zeros(1,length(ref8)+1);
        for i=1:length(b3)
            c3(ref8==b3(i))=a3(i);
        end
         if b3(end) == 2^8
            c3(end) = a3(end);
        end
    end
% LBP 8,1
    filtR=generateRadialFilterLBP(8,1);
    LBP_vector2 = efficientLBP(img,'filtR',filtR,'isRotInv', false);
    LBP_vector2 = vec2mat(uniformLBP(LBP_vector2,8),64);
    c2 = [];
    for i = [13 32 52]
        for j = [13 32 52]
            M = LBP_vector2(i-12:i+12,j-12:j+12);
            [ai,bi] = hist(M(:),unique(M(:)));
            if length(bi)==(length(ref8)+1)
                c2 = [c2,ai];
            else
                ci = zeros(1,length(ref8)+1);
                for k=1:length(bi)
                    ci(ref8==bi(k))=ai(k);
                end
                if bi(end) == 2^8
                    ci(end) = ai(end);
                end
                c2 = [c2,ci];
            end
        end
    end    
    LBP_feature=[c1,c2,c3];
end

