function [ err , feature ] = img_LBP_feature( path2file)
%  Crop & normalize face region before computing the LBP feature
    err = 0;
    [path,name,ext] = fileparts(path2file);
    crop_folder_path = strcat(path,'/crop/');
    crop_folder_path2 = strcat(path,'/crop2/');
    if (~exist(crop_folder_path,'dir'))
        mkdir(crop_folder_path);
    end
    if (~exist(crop_folder_path2,'dir'))
        mkdir(crop_folder_path2);
    end
    crop_save_path = strcat(crop_folder_path,name,ext);
    crop_save_path2 = strcat(crop_folder_path2,name,ext);
    b_defaut=[1110,1815,1856,2608];
    if (~exist(crop_save_path,'file'))
        IMG = imread(path2file);
        rotation = -90;
        img = imrotate(IMG,rotation);
        img = padarray(img,[50 50]);
        %% eyes
        faceDetector = vision.CascadeObjectDetector('EyePairBig'); 
        BB = step(faceDetector,img);
        aire = BB(:,3).*BB(:,4);
        i_tmp = find(aire==max(aire));
        faceDetector = vision.CascadeObjectDetector; 
        BB1 = step(faceDetector,img);
        aire = BB1(:,3).*BB1(:,4);
        j_tmp = find(aire==max(aire));
        
        
        if (isempty(j_tmp))&&(isempty(i_tmp))
            crop_face = imcrop(img,b_defaut);
        elseif (isempty(i_tmp))
            b_face = BB1(j_tmp,:);
            if (max(b_face(:,3).*b_face(:,4))<5000000)
                crop_face = imcrop(img,b_defaut);
            else
                crop_face = imcrop(img,b_face);
            end                
        elseif (isempty(j_tmp))
            b_eye = BB(i_tmp,:);
            if max(b_eye(:,3).*b_eye(:,4))<750000
                crop_face = imcrop(img,b_defaut);
            else
                b_final = [b_eye(1),b_eye(2)-round(b_eye(4)/2),b_eye(3),b_eye(3)+round(b_eye(4)/2)];
                crop_face = imcrop(img,b_final);
            end
        else 
            b_face = BB1(j_tmp,:);
            b_eye = BB(i_tmp,:);
            if ((max(b_face(:,3).*b_face(:,4))<min(max(b_eye(:,3).*b_eye(:,4)),5000000)))
                if b_eye(3)*b_eye(4)<750000
                    crop_face = imcrop(img,b_defaut);
                else
                    b_final = [b_eye(1),b_eye(2)-round(b_eye(4)/2),b_eye(3),b_eye(3)+round(b_eye(4)/2)];
                    crop_face = imcrop(img,b_final);
                end
            else
                if max(b_eye(:,3).*b_eye(:,4))<750000
                    crop_face = imcrop(img,b_face);
                else
                    b_final = [b_eye(1),b_face(2),b_eye(3),b_face(4)];
                    crop_face = imcrop(img,b_final);
                end
            end
        end
            
        crop_face = imresize(crop_face,[64,64]);
        imwrite(crop_face,crop_save_path);
    else
        crop_face = imread(crop_save_path);
    end
    feature = LBPfeature(crop_face);
   

%
% 
%  