paramsFromCsv(CSV_CONFIG_FILE,DATABASE); 
ROOT_DATA_FOLDER = strcat(ROOT_DATA,FOLDER,'_CROP',DELIMITER);
saveFileName = strcat(ROOT_SAVE_BASE,num2str(DATABASE),'_',FOLDER,POSTFIX,'_LBP.mat');
if exist(saveFileName,'file')
    load(saveFileName);
else
    sumNbfiles = sum(NB_FILE);
    features = zeros(sumNbfiles,833);
    labels = zeros(sumNbfiles,1);
     
    Ind = false(sumNbfiles,1);
    Ind_dataset = 0;
    for datasetIndex = 1:length(NB_FILE)
        Nbfile = NB_FILE(datasetIndex); 
        disp(strcat('Load data-set #',num2str(datasetIndex)));
        parfor_progress(Nbfile);
        EXT_FILE = EXT(end-2:end);
        parfor ind = 1:Nbfile
            if PROD
                currentFile = strcat(ROOT_DATA_FOLDER,SUB_FOLDER{datasetIndex},DELIMITER,'img (',num2str(ind),').JPG');
            else
                currentFile = strcat(ROOT_DATA_FOLDER,SUB_FOLDER{datasetIndex},DELIMITER,EXT,DELIMITER,'img (',num2str(ind),').',EXT_FILE);
            end
            if (exist(currentFile,'file'))
                if strcmp(EXT,'dng')
                    disp('Erreur: DNG format');
                else
                    err=0;
%                     [err,feat] = img_LBP_feature(currentFile);
                    feat = LBPfeature(currentFile);
                end
                    
                if err==0
                    features(Ind_dataset+ind,:) = feat;
                    labels(Ind_dataset+ind) = LABEL(datasetIndex);
                    Ind(Ind_dataset+ind) = 1;
                else
                    disp(strcat('Invalid:',num2str(ind),'######################################'));
                    disp(strcat('Invalid:',num2str(ind),'######################################'));
                    disp(strcat('Invalid:',num2str(ind),'######################################'));
                    Ind(Ind_dataset+ind) = 0;
                end
            else
                disp(strcat('Not found:',num2str(ind),'######################################'));
                disp(strcat('Not found:',num2str(ind),'######################################'));
                disp(strcat('Not found:',num2str(ind),'######################################'));
                Ind(Ind_dataset+ind) = 0;
            end 
            parfor_progress;
        end
        parfor_progress(0);
        Ind_dataset = Ind_dataset+Nbfile;        
    end
    Ind = logical(Ind);
    features = features(Ind,:);
    labels = labels(Ind,:);
    save(saveFileName,'features','labels') ;
end
data_labels = DATABASE.*ones(length(labels),1);