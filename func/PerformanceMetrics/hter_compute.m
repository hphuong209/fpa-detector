function [ hter ] = hter_compute( LRs,Labels,tau )
%HTER_COMPUTE Summary of this function goes here
%   Detailed explanation goes here
    ref = ~(isnan(LRs)|isinf(LRs));
    disp(strcat('HTER compute; number of NaN: ',num2str(length(ref)-sum(ref))));
    LRs=LRs(ref);
    Labels=Labels(ref);

    TP = sum(LRs(Labels==0)<tau);
    TN = sum(LRs(Labels==1)>tau);
    FN = sum(LRs(Labels==0)>tau);
    FP = sum(LRs(Labels==1)<tau);
    FAR = FP/(FP+TN);
    FRR = FN/(TP+FN);
    hter = 0.5*(FAR+FRR);
end

