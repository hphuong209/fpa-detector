function [ eer, tau ] = eer_compute( LRs,Labels )
%EER_COMPUTE Summary of this function goes here
%   Detailed explanation goes here
    ref = ~(isnan(LRs)|isinf(LRs));
    disp(strcat('EER compute; number of NaN: ',num2str(length(ref)-sum(ref))));
    LRs=LRs(ref);
    Labels=Labels(ref);

    pas=2*length(LRs);
    FAR = zeros(1,pas);
    FRR = zeros(1,pas);
    Ind=0;
    minThresh =min(LRs);
    maxThresh =max(LRs);
    threshold_tau = minThresh:(maxThresh-minThresh)/pas:maxThresh;
    parfor_progress(pas);
    parfor Ind = 1:length(threshold_tau)
        %                 TP = sum(LRs(Labels==0)>threshold(Ind));
        %                 TN = sum(LRs(Labels==1)<threshold(Ind));
        %                 FN = sum(LRs(Labels==0)<threshold(Ind));
        %                 FP = sum(LRs(Labels==1)>threshold(Ind));
        TP = sum(LRs(Labels==0)<threshold_tau(Ind));
        TN = sum(LRs(Labels==1)>threshold_tau(Ind));
        FN = sum(LRs(Labels==0)>threshold_tau(Ind));
        FP = sum(LRs(Labels==1)<threshold_tau(Ind));
        FAR(Ind) = FP/(FP+TN);
        FRR(Ind) = FN/(TP+FN);
        parfor_progress;
    end
    parfor_progress(0);
    
    tmp = abs(FAR-FRR);
    tmp = tmp==min(tmp);
    
    tau = mean(threshold_tau(tmp));
    eer = (mean(FAR(tmp))+mean(FRR(tmp)))/2; 
end

