function [ X , Y ] = compute_ROC( LRs,Labels,action)
%COMPUTE_ROC Summary of this function goes here
%   Detailed explanation goes here

    ref = ~(isnan(LRs)|isinf(LRs));
    disp(strcat('number of NaN: ',num2str(length(ref)-sum(ref))));
    LRs=LRs(ref);
    Labels=Labels(ref);

    Labels =Labels>0;
    pas=2*length(LRs);
    X = zeros(1,pas);
    Y = zeros(1,pas);
    Ind=0;
    minThresh =min(LRs);
    maxThresh =max(LRs);
    threshold = minThresh:(maxThresh-minThresh)/pas:maxThresh;
    parfor_progress(pas);
    parfor Ind = 1:length(threshold)
        if (action==1)
                TP = sum(LRs(Labels==0)>threshold(Ind));
                TN = sum(LRs(Labels==1)<threshold(Ind));
                FN = sum(LRs(Labels==0)<threshold(Ind));
                FP = sum(LRs(Labels==1)>threshold(Ind));
        end
        if (action==2)
                TP = sum(LRs(Labels==0)<threshold(Ind));
                TN = sum(LRs(Labels==1)>threshold(Ind));
                FN = sum(LRs(Labels==0)>threshold(Ind));
                FP = sum(LRs(Labels==1)<threshold(Ind));
        end
        X(Ind) = FP/(FP+TN);
        Y(Ind) = TP/(TP+FN);
        parfor_progress;
    end
    parfor_progress(0);

end

