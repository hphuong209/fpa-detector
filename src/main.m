clear all;
close all;
CHECK_PREVIEW = false;

ROOT_DATA = '/path/to/database/'
ROOT_SAVE = '/path/to/save/computed/data/';

ROOT_SAVE_BASE = strcat(ROOT_SAVE,'base/');
ROOT_SAVE_TEST = strcat(ROOT_SAVE,'test/');
if ~exist(ROOT_SAVE_BASE,'dir')
    mkdir(ROOT_SAVE_BASE);
    mkdir(ROOT_SAVE_TEST);
end
% path to csv file which describe the whole database
CSV_CONFIG_FILE = strcat(ROOT_DATA,'database_configuration.csv');
DELIMITER='/';

inverse_Gamma = false;
if inverse_Gamma
     GAMMA_COEF = 2.4;
else
    GAMMA_COEF = 0;
end
% Selection of color channel
COLOR_CHANNEL = 1;
% Image extention
EXT='JPG';
% Postfix of saved file name
POSTFIX = strcat('_C',num2str(COLOR_CHANNEL),'_',EXT);

% Index of datasets involve in the study
DATABASES = 1:9;
%% Data extraction

for k_ind=1:length(DATABASES)
    disp(strcat('###Process database #',num2str(DATABASES(k_ind))));
    DATABASE = DATABASES(k_ind);
    disp('Extract NLV Data');
    % Extract NLV data
    NLV_data_extraction;
end

%% Classification
% Configuration of training datasets
TRAIN = [ 2 , 4];
% Testing datasets
TEST = setdiff(DATABASES,TRAIN);
% Process the proposed solution
main_proposed;
% Process the solution proposed by Matta et al.
LBP=1;
main_others;
% Process the solution proposed by Zinelabidine et al.
LBP=0;
main_others;
legend(['Proposed solution','Matta - LBP','Zine - '+DESC_TYPEs,]);
