%% Start
if LOADING_H0    
    H0_model_estimation;
else
    paramsFromCsv(CSV_CONFIG_FILE,DATABASE); 
    ROOT_DATA_FOLDER = strcat(ROOT_DATA,FOLDER,DELIMITER);
    loadingFileName = strcat(ROOT_SAVE_BASE,num2str(DATABASE),'_',FOLDER,POSTFIX,'.mat');
    savefile = strcat(ROOT_SAVE_TEST,num2str(DATABASE),'_',FOLDER,POSTFIX,POSTFIX_TEST,'_linear.mat');
    if ~exist(savefile,'file')
        if ~exist(loadingFileName,'file')
            disp('-------------------')
            error('Ouf! General data not exist!')  
        else
            load(loadingFileName);
            Lr = zeros(length(labels),1);
            NB = zeros(length(labels),NB_LEVESET_TO_COMPUTE);
            Sample_size = zeros(length(labels),NB_LEVESET);
            parfor_progress(length(labels));
            parfor i = 1:length(labels)
                if ON_NOISE
                    nlv = nlv_noise_s{i}
                else
                    nlv  = nlv_img_s{i};
                end
                smooth_esp = smooth_esp_s{i};
                skin = skin_s{i};
                content_var = content_var_s{i};
                selection_mask = (content_var<UNIFORM_THRESHOLD)&skin;
                levelset_mask_nb = zeros(NB_LEVESET,1);
                nlv_by_levelset = cell(NB_LEVESET,1);

                for j=1:NB_LEVESET
                    level_mask = selection_mask & smooth_esp>=LEVELSET_WIDTH*(j-1) & smooth_esp<LEVELSET_WIDTH*j;
                    nlv_by_levelset{j} = nlv(level_mask);
                    levelset_mask_nb(j) = length(nlv_by_levelset{j});
                end
                Sample_size(i,:) = levelset_mask_nb';



                if ~isempty(LEVESET_TO_COMPUTE)
                    indice_levelset_selected = LEVESET_TO_COMPUTE;
                else
                    levelset_mask_nb_sorted = sort(levelset_mask_nb);
                    indice_levelset_selected = find(levelset_mask_nb>=100);
                    indice_levelset_selected = indice_levelset_selected(indice_levelset_selected>=(0.25*NB_LEVESET)&indice_levelset_selected<=(0.75*NB_LEVESET));

                    tmp = [levelset_mask_nb(indice_levelset_selected),indice_levelset_selected];
                    tmp = sortrows(tmp,1);                            
                    indice_levelset_selected = tmp(end+1-NB_LEVESET_TO_COMPUTE:end,2);
                end
                samples = nlv_by_levelset(indice_levelset_selected); 
                NB(i,:) = sort(levelset_mask_nb(indice_levelset_selected))';
                Lr(i) = qStatistiqueForLinearRelation(samples,P0(indice_levelset_selected,1),P0(indice_levelset_selected,2));
                parfor_progress;
            end
            parfor_progress(0);
        end
        save(savefile,'NB','Lr','labels','Sample_size');
    else
        load(savefile);
    end
end

%%
return
figure; hold on;
for i  = 1:length(Sample_size)
    plot(Sample_size(i,:),'Color',[rand() rand() rand()]);
end
