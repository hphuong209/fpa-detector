paramsFromCsv(CSV_CONFIG_FILE,DATABASE);
ROOT_DATA_FOLDER = strcat(ROOT_DATA,FOLDER,DELIMITER);
saveFileName = strcat(ROOT_SAVE_BASE,num2str(DATABASE),'_',FOLDER,POSTFIX,'.mat');
disp(SUB_FOLDER);
%% Deductive parameter
if ~exist(saveFileName,'file')
    sumNbfiles = sum(NB_FILE);
    nlv_noise_s = cell(sumNbfiles,1);
    nlv_img_s = cell(sumNbfiles,1);
    skin_s = cell(sumNbfiles,1);
    smooth_esp_s = cell(sumNbfiles,1);
    content_var_s = cell(sumNbfiles,1);
    labels = zeros(sumNbfiles,1);
    Ind = false(sumNbfiles,1);
    Ind_dataset = 0;
    for datasetIndex = 1:length(NB_FILE)
        Nbfile = NB_FILE(datasetIndex);
        disp(strcat('Load data-set #',num2str(datasetIndex)));
        parfor_progress(Nbfile);
        parfor ind = 1:Nbfile
            if PROD
                currentFile = strcat(ROOT_DATA_FOLDER,SUB_FOLDER{datasetIndex},DELIMITER,'img (',num2str(ind),').JPG');
            else
                currentFile = strcat(ROOT_DATA_FOLDER,SUB_FOLDER{datasetIndex},DELIMITER,EXT,DELIMITER,'img (',num2str(ind),').',EXT_FILE);
            end
            if (exist(currentFile,'file'))
                if strcmp(EXT,'dng')
                    [nlv_noise,nlv_img,skin,smooth_esp,content_var] = rgb_dng_feature(currentFile,COLOR_CHANNEL);
                else
                    [nlv_noise,nlv_img,skin,smooth_esp,content_var] = rgb_jpg_feature(currentFile,COLOR_CHANNEL,GAMMA_COEF);
                end
                    
                nlv_noise_s{Ind_dataset+ind} = nlv_noise;
                nlv_img_s{Ind_dataset+ind} = nlv_img;
                skin_s{Ind_dataset+ind} = skin;
                smooth_esp_s{Ind_dataset+ind} = smooth_esp;
                content_var_s{Ind_dataset+ind} = content_var;
                labels(Ind_dataset+ind) = LABEL(datasetIndex);
                Ind(Ind_dataset+ind) = 1;
            else
                disp(strcat('err: ',currentFile,' is not found!'));
                Ind(Ind_dataset+ind) = 0;
            end 
            parfor_progress;
        end
        parfor_progress(0);
        Ind_dataset = Ind_dataset+Nbfile;        
    end
    nlv_noise_s = nlv_noise_s(Ind);
    nlv_img_s = nlv_img_s(Ind);
    skin_s = skin_s(Ind);
    smooth_esp_s = smooth_esp_s(Ind);
    content_var_s = content_var_s(Ind);
    labels = labels(Ind);
    save(saveFileName,'nlv_noise_s','nlv_img_s','skin_s','smooth_esp_s','content_var_s','labels','-v7.3') ;
else
    disp('Loading Precalculated data');
%     load(saveFileName);
end