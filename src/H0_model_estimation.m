    paramsFromCsv(CSV_CONFIG_FILE,DATABASE_H0);
    loadingFileName = strcat(ROOT_SAVE_BASE,num2str(DATABASE_H0),'_',FOLDER,POSTFIX,'.mat');
    if (UNIFORM_THRESHOLD ~= 1000);
        saveFileName = strcat(ROOT_SAVE_TEST,num2str(DATABASE_H0),'_',FOLDER,POSTFIX,'_H0_CD',num2str(NB_LEVESET),'_',num2str(UNIFORM_THRESHOLD),'.mat');
    else
        saveFileName = strcat(ROOT_SAVE_TEST,num2str(DATABASE_H0),'_',FOLDER,POSTFIX,'_H0_CD',num2str(NB_LEVESET),'.mat');
    end
    if exist(saveFileName,'file')
        load(saveFileName);
    else
        load(loadingFileName);
        % TODO load data first
        nlv_by_levelset = cell(NB_LEVESET,1);
        nlv_noise = nlv_noise_s(labels==0);
        nlv_img = nlv_img_s(labels==0);
        smooth_esp_tmp = smooth_esp_s(labels==0);
        skin_tmp = skin_s(labels==0);
        content_var_tmp = content_var_s(labels==0);
        parfor_progress(sum(labels==0));
        for i = 1:sum(labels==0)
            if ON_NOISE
                nlv = nlv_noise{i};
            else
                nlv  = nlv_img{i};
            end
            smooth_esp = smooth_esp_tmp{i};
            skin = skin_tmp{i};
            content_var = content_var_tmp{i};
            selection_mask = (content_var<UNIFORM_THRESHOLD)&skin;
            parfor j=1:NB_LEVESET
                level_mask = selection_mask & smooth_esp>=LEVELSET_WIDTH*(j-1) & smooth_esp<LEVELSET_WIDTH*j;
                nlv_inlevelset = nlv(level_mask);
                nlv_by_levelset{j} = [nlv_by_levelset{j};nlv_inlevelset];
            end
            parfor_progress;
        end
        parfor_progress(0);
        P0 = zeros(NB_LEVESET,2);
        for j = 1:NB_LEVESET
            tmp_nlvs = nlv_by_levelset{j};
            if length(tmp_nlvs)>5000
                nb_sample = floor(length(tmp_nlvs)/500);
                p_gammas = zeros(nb_sample,2);
                parfor k=1:nb_sample
                    if (k == nb_sample)
                        sub_sample = tmp_nlvs((500*(k-1)+1):end);
                    else
                        sub_sample = tmp_nlvs((500*(k-1)+1):500*k);
                    end
                    p_gammas(k,:) = gamfit(sub_sample);
                end
                P0(j,:) = polyfit(1./p_gammas(:,2),p_gammas(:,1),1);     
            end
        end
        save(saveFileName,'P0');
    end