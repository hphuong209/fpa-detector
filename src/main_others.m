if LBP==1
    script ='compute_features_for_LBP';
    DESC_TYPEs = 1;
elseif LBP==0
    script ='compute_features_for_Zine';
    DESC_TYPEs =[string('LBP'),string('CoALBP'),string('LPQ'),string('BSIF')];
end
disp('------------------------------');
COLORS =[45 158 37;29 124 120;74 29 124;193 29 105]/256;
for k=1:length(DESC_TYPEs)
    if LBP==0
        DESC_TYPE = DESC_TYPEs(k);
    end
    DATABASE = DATABASES(1);
%     disp(strcat('Compute test-statistic #',num2str(DATABASE)));
    eval(script);
    Features_lbp = features;
    Labels_lbp = labels;
    Labels_data = data_labels;
    for i=2:length(DATABASES)
        DATABASE = DATABASES(i);
%         disp(strcat('Compute test-statistic #',num2str(DATABASE)));
        eval(script);
        Features_lbp = [Features_lbp;features];
        Labels_lbp = [Labels_lbp;labels];
        Labels_data = [Labels_data;data_labels];
    end

    prepareData4SVM = [Labels_lbp,Features_lbp];
    train_base = Features_lbp(ismember(Labels_data,TRAIN),:);
    train_label = Labels_lbp(ismember(Labels_data,TRAIN));
    test_base = Features_lbp(ismember(Labels_data,TEST),:);
    test_label = Labels_lbp(ismember(Labels_data,TEST));

    sv = fitcsvm(train_base,train_label,'KernelScale','auto','Standardize',true,...
        'OutlierFraction',0.01);
    
    %% Training evaluation EER    
    [~,Score_train]  = predict(sv,train_base);
    [eer,tau] = eer_compute(Score_train(:,2),train_label);
%     [eer_b,tau_b] = eer_compute(Score_train(:,2),label_train);
    %% HTER
    [~,Score]  = predict(sv,test_base);
    hter = hter_compute(Score(:,2),test_label,tau);
%     hter_b = hter_compute(Score(:,2),label,tau_b);
    %% ROC
    [X_lbp,Y_lbp,T,AUC] = perfcurve(test_label,Score(:,1),0);
    %figure;
    if LBP
        l_color = [0 0 1];
    else
        l_color = COLORS(k,:);
    end
    semilogx(X_lbp,Y_lbp,'Color',l_color);
    if LBP==0
        fprintf('%s - %s',script,DESC_TYPE);
    else
        fprintf('%s - LBP',script);
    end
    fprintf('\nRef: \n\t EER:\t%0.3f\n\t HTER %0.3f\n\t tau:\t%0.3f \n ',eer,hter,tau);
%     fprintf('Bis\n\t EER:\t%0.3f\n\t HTER %0.3f\n\t tau:\t%0.3f ',eer_b,hter_b,tau_b);

    
end
return

