%% Hypothesis test
ON_NOISE = true;
NB_LEVESET = 100;
LEVELSET_WIDTH = 1/NB_LEVESET;
UNIFORM_THRESHOLD = 0.002;
NB_LEVESET_TO_COMPUTEs = 10;
Z_nb= length(NB_LEVESET_TO_COMPUTEs)
%%
color = {'r','g','b','m','k','c','y'};
roc_data = cell(Z_nb,0);
HO_SUFFIX =num2str(DATABASE_H0);
%% Load H0
disp('Estimation of the model parameter under H0'); 
H0_model_estimation;
%% Start
for k = 1:Z_nb
    NB_LEVESET_TO_COMPUTE = NB_LEVESET_TO_COMPUTEs(k);
    POSTFIX_TEST = strcat('_',num2str(NB_LEVESET),'-',HO_SUFFIX,'_',num2str(NB_LEVESET_TO_COMPUTE),'_',strrep(num2str(UNIFORM_THRESHOLD),'.','-'))
    MULTIPLE = 5;
    script = 'test_linear_relation_m';
    %% Loading the model parameter under H0
    LOADING_H0=1; %#ok<NASGU>
    disp('Loading the model parameter under H0');
    eval(script);
    %% Computing the Likelihood Ratio on the Training set
    LOADING_H0=0;
    clearvars LRs Labels LRs_train Labels_train 
    for i=1:length(TRAIN)
        DATABASE = TRAIN(i);
        eval(script);
        if exist('LRs_train')
            LRs_train = [LRs_train;Lr];
            Labels_train = [Labels_train;labels];
        else
            LRs_train = Lr;
            Labels_train = labels;
        end
    end
    %% Computing the Likelihood Ratio on the Testing set
    for i=1:length(TEST)
        DATABASE = TEST(i);
        eval(script);
        if exist('LRs')
            LRs = [LRs;Lr];
            Labels = [Labels;labels];
        else
            LRs = Lr;
            Labels = labels;
        end
    end
    %% EER computation
    [ eer, tau_eer ] = eer_compute(LRs_train,Labels_train);
    %% HTER Computation
    [ hter ] = hter_compute(LRs,Labels,tau_eer);
    [ X Y ] = compute_ROC(LRs,Labels);
    roc_data{k,1}=X;
    roc_data{k,2}=Y;
    roc_data{k,3}= [ eer , hter , tau_eer];
end
h=figure(); 
for k = 1:Z_nb
    m = roc_data{k,3};
    fprintf('%d level-sets: \n\t EER:\t%0.3f\n\t HTER %0.3f\n\t tau:\t%0.3f ',NB_LEVESET_TO_COMPUTEs(k),m(1),m(2),m(3));
    X=roc_data{k,1};
    Y=roc_data{k,2};
    semilogx(X,Y,'Color',color{k},'LineWidth',1);  
    hold on;
end
grid on
legend(NB_LEVESET_TO_COMPUTEs+string(' level sets'));
xlabel('FPR');
ylabel('TPR');
xlim([0.0005,1]);
title('ROC');
hold on;
%%
% figure;
% Lr1 = LRs(Labels~=0);
% [x,y]=dataT(Lr1,50)
% hold on
% plot(x,y,'b')
% Lr0 = LRs(Labels==0);
% [x,y]=dataT(Lr0,50)
% plot(x,y,'r')
% legend('LR1','LR0');
% title('LR0 vs LR1');
% %%
% figure;
% hold on
% plot(x,y,'r')
% x= 1:0.1:100;
% y_c = chi2pdf(x,NB_LEVESET_TO_COMPUTEs(end));
% plot(x,y_c,'b')
% legend('LR1','theorique');
% title('LR0 vs theorique');
